# pipeline-demo
This project contains a prototype of CD pipeline that could be used in Pega project.

## Pipeline
Pipeline is implemented using GitLab's built-in [feature](https://gitlab.com/help/ci/quick_start/README) 
and could be found under ``Pipelines`` tab, it has three stages:
* export
* import
* commit

![pipeline.png](img/pipeline.png)

The ``export`` stage consists of one job ``export-archive`` run automatically.

The ``import`` stage consists of two jobs ``deploy-to-env1`` and ``deploy-to-env2`` 
each linked to corresponding environment. These jobs are manual, user can 
decide which environment to deploy to and select a job to run.

After deploying application to specified environment a user has an option either 
to commit or rollback deployment.
This is implemented by the ``commit`` stage having four jobs ``commit-env1``, 
``rollback-env1``, ``commit-env2``, ``rollback-env2``.

For complete pipeline definition please refer to [.gitlab-ci.yml](.gitlab-ci.yml).

User can also track which application builds were deployed to an environment.

![env2.png](img/env2.png)

## Release automation tool
As a release automation tool you can use both [prpcServiceUtils](https://pdn.pega.com/exchange/components/remote-configuration-command-line-tool-prpcserviceutils) 
and [Ninja Release Automation](https://pegadevops.com).

An advantage of ``prpcServiceUtils`` is that it is a free tool recommended by Pegasystems, 
the main disadvantage, however, is that it requires tons of configuration on the 
side of build server to implement a pipeline.

An advantage of ``Ninja Release Automation`` is that it is a comprehensive tool 
allowing to implement pipeline with a minimal configuration and providing such features as:
* composite distribution package (Pega-archive, SQL-scripts, etc.);
* environment properties customization;
* 'one-click' deployment.


However, this tool is not free for use.
